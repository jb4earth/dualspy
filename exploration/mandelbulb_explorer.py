import numpy as np
np.warnings.filterwarnings('ignore')

import pyvista as pv
import open3d as o3

def gridgen(points):
    x_ = np.linspace(-1, 1, points)
    y_ = np.linspace(-1, 1, points)
    z_ = np.linspace(-1, 1, points)

    x, y, z = np.meshgrid(x_, y_, z_, indexing='ij')

    return np.array([x.flatten(),y.flatten(),z.flatten()]).T

def get_rad(v): return np.sqrt(v[:,0]**2+v[:,1]**2+v[:,2]**2)

def get_phi(v): return np.arctan(v[:,1]/v[:,0])

def get_the(v): return np.arctan(np.sqrt(v[:,0]**2+v[:,1]**2)/v[:,2])

def mandelbulb_iter(v,n=[2,2,2]):
    radius = get_rad(v)
    phi = get_phi(v)
    theta = get_the(v)

    x = radius**n[0]*np.sin(theta*n[0])*np.cos(phi*n[0])
    y = radius**n[1]*np.sin(phi*n[1])*np.sin(theta*n[1])
    z = radius**n[2]*np.cos(theta*n[2])

    return np.array([x,y,z]).T

def dropnans(i10,mm=2):
    i10[i10>mm] = np.NaN
    i10[i10<-mm] = np.NaN
    return i10[~np.isnan(i10).any(axis=1)]


def mandelpoints(iters,points,n=[3,3,3]):
    for i in range(iters):
        points += mandelbulb_iter(points,n)
        points = dropnans(points)
    return points


def mandelmesh(bulb,thick=0.3,smooth=150,decimate=0.5):
    dbulb = bulb*0 # create matching zero vector

    # create cube
    cubecorners = np.array([[1,1,1],[1,1,-1],[1,-1,1],[-1,1,1],[-1,-1,1],[-1,1,-1],[1,-1,-1],[-1,-1,-1]])
    s = thick*cubecorners # scale cubes

    # cubify mandelbulb (it's a pointcloud with no-normals and these are needed by Poisson reconstruction)
    ex = np.vstack([bulb+s[0],bulb+s[1],bulb+s[2],bulb+s[3],bulb+s[4],bulb+s[5],bulb+s[6],bulb+s[7]])

    # convert to open3d Point Cloud
    pcd = o3.geometry.PointCloud()
    pcd.points = o3.utility.Vector3dVector(ex)

    # set normals
    s = s*(1/thick)
    nnn =  np.vstack([dbulb+s[0],dbulb+s[1],dbulb+s[2],dbulb+s[3],dbulb+s[4],dbulb+s[5],dbulb+s[6],dbulb+s[7]])
    pcd.normals = o3.utility.Vector3dVector(nnn)
    pcd.normalize_normals()

    # create mesh from points (poisson reconstruction)
    test,_ = o3.geometry.TriangleMesh.create_from_point_cloud_poisson(pcd,depth=6,scale=2.25)

    # transfer to pyvista for cleaning and viewing
    o3.io.write_triangle_mesh('Temp.ply',test)
    mesh = pv.read('Temp.ply')

    # pyvista cleaning
    mesh.extract_largest(True)
    mesh = mesh.decimate(decimate)
    mesh = mesh.smooth(n_iter=smooth)
    return mesh

def gen_mandel(va,vb,vc,it,offset=[0,0,0],gridsize=50):
    points = mandelpoints(it,gridgen(gridsize),[va,vb,vc])
    points = points - points.mean(axis=0) #center
    points = points * (1/abs(points).max(axis=0)) # scale to cubeish size
    points = points + np.array(offset)
    return mandelmesh(points,0.15,40,0)





class MandelbulbPlotEngine():
    def __init__(self, mesh):
        self.output = mesh # Expected PyVista mesh type
        # default parameters
        self.kwargs = {
            'va': 0,
            'vb': 0,
            'vc': 0,
            'it': 1,
        }

    def __call__(self, param, value):
        self.kwargs[param] = value
        self.update()

    def update(self):
        # This is where you call your simulation
        result = gen_mandel(**self.kwargs)
        print(self.kwargs)
        self.output.overwrite(result)
        return


starting_mesh = pv.Sphere()
engine = MandelbulbPlotEngine(starting_mesh)

slide_pad_y = 0.066
v_range = [-6,6]
bx = .975
pv.set_plot_theme("document")

p = pv.Plotter(notebook=False)
p.store_image = True
# pv.set_background((0,0,0))
p.add_mesh(starting_mesh, show_edges=False, color=True)
p.add_slider_widget(
    callback=lambda value: engine('va', (value)),
    rng=v_range,
    value=0,
    title="",
    pointa=(.025, .1), pointb=(bx, .1),
)
p.add_slider_widget(
    callback=lambda value: engine('vb', (value)),
    rng=v_range,
    value=0,
    title="",
    pointa=(.025, .1+slide_pad_y), pointb=(bx, .1+slide_pad_y),
)
p.add_slider_widget(
    callback=lambda value: engine('vc', (value)),
    rng=v_range,
    value=0,
    title="",
    pointa=(.025, .1+slide_pad_y*2), pointb=(bx, 0.1+slide_pad_y*2),
)
p.add_slider_widget(
    callback=lambda value: engine('it', int(value)),
    rng=[0,16],
    value=3,
    title="",
    pointa=(.025, .1+slide_pad_y*3), pointb=(bx, 0.1+slide_pad_y*3),
)
p.show()
