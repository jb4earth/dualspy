import sys
sys.path.append("../.")


from gen_mandelbulb import *
import pymeshfix as mf
import pyvista as pv
import itkwidgets
import math

from progress.bar import FillingSquaresBar
import warnings
warnings.filterwarnings('ignore')

import os, contextlib

def supress_stdout(func):
    def wrapper(*a, **ka):
        with open(os.devnull, 'w') as devnull:
            with contextlib.redirect_stdout(devnull):
                func(*a, **ka)
    return wrapper



# @supress_stdout
def gen_stl(Va,Vb,Vc,iters):
#     rm_file('Temp.stl')

    points = mandelpoints(iters,gridgen(100),[Va,Vb,Vc])
    points = points - points.mean(axis=0) #center
    points = points * (1/abs(points).max(axis=0)) # scale to cubeish size
    mesh =  mandelmesh(points,0.15,50,0.9)


#     _meshfix.clean_from_file('Temp.stl','Temp.stl')
    meshfix = mf.MeshFix(mesh)
    meshfix.repair(verbose=True)
    return meshfix.mesh

# meshfix.mesh.plot(cpos=cpos)
num = 9
hi =  2.5
lo = -2.5
i  = 0
bar = FillingSquaresBar(max = num**3)

for x in np.linspace(lo,hi,num):
    for y in np.linspace(lo,hi,num):
        for z in np.linspace(lo,hi,num):
            i += 1
            name = str(i).zfill(len(str(num**3))) #fill to the number of required places
#             print(name,x,y,z)
            mesh = gen_stl(x,y,z,3)
            # mesh = pv.read('Temp.stl')
            pv.save_meshio(name+'.stl',mesh)
            # print('\r'+str(round(i/(num**3)*100,2)).zfill(5)+'%')
            bar.next()
