import lxml
from lxml import etree as et
from lxml.etree import Element as el
from lxml.etree import SubElement as sub

########################################################################3
# lxml functions
# this is a dirty hacky way but it feels so nice to type

def n(var):    exec(var+'=\"'+var+'\"',globals())

def variablename(var):
    return str([tpl[0] for tpl in filter(lambda x: var is x[1], globals().items())][0])

def mc(bt,child):
    n(child)
    toex = child+' = sub('+variablename(bt)+','+'"'+child+'"'+')'
    exec(toex,globals())

#########################################################################
# XML Parsing for required components

def parameters(domain_size,max_time=1,save_time=0.1):
    mc(execution,'parameters')

    sub(parameters,'parameter', key="SavePosDouble" ,value="0")
    sub(parameters,'parameter', key="StepAlgorithm" ,value="2")
    sub(parameters,'parameter', key="VerletSteps" ,value="40")
    sub(parameters,'parameter', key="Kernel" ,value="2")
    sub(parameters,'parameter', key="ViscoTreatment" ,value="1")
    sub(parameters,'parameter', key="Visco" ,value="0.01")
    sub(parameters,'parameter', key="ViscoBoundFactor" ,value="0")
    sub(parameters,'parameter', key="DensityDT" ,value="2")
    sub(parameters,'parameter', key="DensityDTvalue" ,value="0.1")
    sub(parameters,'parameter', key="Shifting" ,value="0")
    sub(parameters,'parameter', key="ShiftCoef" ,value="-2")
    sub(parameters,'parameter', key="ShiftTFS" ,value="1.5")
    sub(parameters,'parameter', key="RigidAlgorithm" ,value="1")
    sub(parameters,'parameter', key="FtPause" ,value="0.0")
    sub(parameters,'parameter', key="CoefDtMin" ,value="0.05")
    sub(parameters,'parameter', key="#DtIni" ,value="0.0001")
    sub(parameters,'parameter', key="#DtMin" ,value="1e-05")
    sub(parameters,'parameter', key="#DtFixed" ,value="DtFixed.dat")
    sub(parameters,'parameter', key="DtAllParticles" ,value="0")
    sub(parameters,'parameter', key="TimeMax" ,value=str(max_time))
    sub(parameters,'parameter', key="TimeOut" ,value=str(save_time))
    sub(parameters,'parameter', key="PartsOutMax" ,value="1")
    sub(parameters,'parameter', key="RhopOutMin" ,value="700")
    sub(parameters,'parameter', key="RhopOutMax" ,value="1300")
    # sub(parameters,'_parameter',key='YPeriodicIncZ',value='0')
    sub(parameters,'parameter',key='XPeriodicIncY',value='0.0')
    mc(parameters,'simulationdomain')
    sub(simulationdomain,'posmin', x=str(-domain_size/2),y=str(-domain_size/2),z=str(-domain_size/2))
    sub(simulationdomain,'posmax', x=str(domain_size),y=str(domain_size),z=str(domain_size))

def special():
    mc(execution,'special')

def init_geo_mode(particle_size,domain_size):
    size = domain_size
    mc(casedef,'geometry')
    mc(geometry, 'definition')
    definition.set('dp',str(particle_size))
    sub(definition, 'pointmin',x=str(-size/2),y=str(-size/2),z=str(-size/2))
    sub(definition, 'pointmax',x=str(size),y=str(size),z=str(size))
    # sub(drawbox,'point',x=str(-size/2),y=str(-size/2),z=str(-size/2))
    # sub(drawbox,'size',x=str(size),y=str(size),z=str(size))
    mc(geometry,'commands')
    mc(commands, 'mainlist')

# define the constants for an open channel
def open_consts():
    sub(constantsdef,'lattice', bound="1", fluid="1")
    sub(constantsdef,'h', value="0", auto="true", units_comment="metres (m)")
    sub(constantsdef,'b', value="0", auto="true", units_comment="metres (m)")
    sub(constantsdef,'massbound', value="0", auto="true", units_comment="kg")
    sub(constantsdef,'massfluid', value="0", auto="true", units_comment="kg")

# standard constants (for all cases maybe)
def define_consts(density,open_channel):
    mc(casedef,'constantsdef')
    sub(constantsdef,'gravity',x='0',y='0',z='-9.81')
    sub(constantsdef,'rhop0',value=str(density), comment="Reference density of the fluid",units_comment="kg/m^3")
    sub(constantsdef,'hswl',value="0",auto='true')
    sub(constantsdef,'gamma',value='7')
    sub(constantsdef,'speedsystem',value='0', auto='true')
    sub(constantsdef,'coefsound',value='20')
    sub(constantsdef,'speedsound',value='0',auto='true')
    sub(constantsdef,'coefh',value='1.0') # comment="Coefficient to calculate the smoothing length (h=coefh*sqrt(3*dp^2) in 3D)"
    sub(constantsdef,'cflnumber',value='0.2')
    if open_channel == 'on':
        open_consts()
        mc(casedef,'mkconfig')
        mkconfig.set('boundcount','240')
        mkconfig.set('fluidcount','10')
        mkconfig.text = '\n'
    else: #not an open channel (i.e. flume)
        mc(casedef,'mkconfig')
        mkconfig.set('boundcount','230')
        mkconfig.set('fluidcount','9')

        sub(mkconfig, 'mkorientfluid',mk='0',orient='Xyz')



def init_case():

    global root
    root = el('case')

    mc(root,'casedef')
    mc(root,'execution')

#########################################################################
# XML for drawing stuff

def load_stl(stl_scale,stl_z=0):
    mc(mainlist,'drawfilestl')
    drawfilestl.set('file',"Temp.stl")
    sub(drawfilestl,'drawmove',x = "0", y='0', z=str(stl_z))
    sub(drawfilestl,'drawscale',x = str(stl_scale), y=str(stl_scale), z=str(stl_scale))

def draw_float(stl_scale,stl_z):
    mc(mainlist, 'setshapemode')
    setshapemode.text = 'dp | bound'
    sub(mainlist,'setdrawmode',mode='full')
    sub(mainlist,'setmkbound',mk='20')
    load_stl(stl_scale,stl_z)
    # sub(mainlist,'shapeout',file='Floater',reset='true')

def draw_flume(dp,fluidsize=2):
    # sub(mainlist,'setdrawmode',mode='full')
    sub(mainlist,'setmkbound',mk='1')
    draw_box(fluidsize+dp/5) # make it bigger than the fluid so it can hold it
    sub(mainlist,'setmkfluid',mk='0')
    draw_fluid(dp,fluidsize)

def draw_fluid(dp,size):
    mc(mainlist,'fillbox')
    fillbox.set('x',str(-size/2+dp))
    fillbox.set('y',str(size/2-dp))
    fillbox.set('z',str(size/2-dp))
    fillbox.set('objname','Water')
    mc(fillbox,'modefill')
    modefill.text = 'void'
    sub(fillbox,'point',x=str(-size/2),y=str(-size/2),z=str(-size/2))
    sub(fillbox,'size',x=str(size),y=str(size),z=str(size))
    sub(mainlist,'shapeout',file="")

def draw_box(size):
    """Warning: flume box is extremely sensitive to changes in size"""
    mc(mainlist,'drawbox')
    drawbox.set('objname','Box')
    mc(drawbox,'boxfill')
    boxfill.text = 'bottom | front | back'
    sub(drawbox,'point',x=str(-size/2),y=str(-size/2),z=str(-size/2))
    sub(drawbox,'size',x=str(size),y=str(size),z=str(size))

def draw_tank():
    sub(mainlist,'setmkbound',mk='0')
    mc(mainlist,'drawprism')
    drawprism.set('mask','5 | 6')
    sub(drawprism,'point', x="80", y="-8", z="7")
    sub(drawprism,'point', x="10", y="-8", z="-8")
    sub(drawprism,'point', x="-40", y="-8", z="-8")
    sub(drawprism,'point', x="-40", y="-8", z="7")
    sub(drawprism,'point', x="80", y="8", z="7")
    sub(drawprism,'point', x="10", y="8", z="-8")
    sub(drawprism,'point', x="-40", y="8", z="-8")
    sub(drawprism,'point', x="-40", y="8", z="7")
    sub(mainlist,'shapeout', file='Tank',reset='true')

    sub(mainlist,'setmkfluid',mk='0')
    mc(mainlist,'fillbox')
    fillbox.set('x','0')
    fillbox.set('y','0')
    fillbox.set('z','-7')
    mc(fillbox,'modefill')
    modefill.text = 'void'
    sub(fillbox,'point',x="-100", y="-20", z="-10" )
    sub(fillbox,'size',x="200", y="40", z="10")
    sub(mainlist,'shapeout', file='Fluid',reset='true')

##########################################################################
# do stuff with drawn objects

def rz_uniform(dp,size_for_location=2,velocity=1):
    size = size_for_location
    mc(special,'relaxationzones')
    mc(relaxationzones,'rzwaves_uniform')
    sub(rzwaves_uniform,'start',value='0') #start time
    sub(rzwaves_uniform,'duration',value='0') #end time - zero is end
    mc(rzwaves_uniform,'domainbox')
    sub(domainbox,'point',x=str(-size/2+dp),y=str(-size/2+dp),z=str(-size/2+dp))
    sub(domainbox,'size', x=str(2*dp),y=str(size-2*dp),z=str(size-2*dp))
    sub(domainbox,'direction',x='1',y='0',z='0')
    mc(domainbox,'_rotateaxis')
    _rotateaxis.set('angle','-45')
    _rotateaxis.set('anglesunits','degrees')
    sub(_rotateaxis, 'point1',x='0.3',y='0',z='0')
    sub(_rotateaxis,'point2',x='0.3',y='1',z='0')
    # original code contained a rotate axis here that seemed useless
    mc(rzwaves_uniform,'velocitytimes')
    sub(velocitytimes,'timevalue',time='0.0',v=str(velocity))
    # can add more times in if you want diff velocities at diff velocitytimes
    sub(rzwaves_uniform,'coefdt',value='1000') #"Multiplies by dt value in the calculation (using 0 is not applied) (default=1000)"
    sub(rzwaves_uniform,'function',psi='0.1',beta='4') # TODO figure out what this actually is
    # above comment = Coefficients in funtion for velocity (def. psi=0.9, beta=1)



def set_floatings(float_density):
    mc(casedef,'floatings')
    sub(floatings,'floating',mkbound='50',relativeweight=str(float_density))

def moorings(max_time,save_time,mooring_length=11.5,mooring_segements=15,connection_z = 0):
    mc(special,'moorings')
    sub(moorings,'savevtk_moorings', value="true")
    sub(moorings,'savecsv_points', value="true")
    sub(moorings,'savevtk_points', value="false")
    mc(moorings,'mooredfloatings')
    sub(mooredfloatings,'floating',mkbound='50')

    mc(moorings,'moordyn')
    mc(moordyn,'solverOptions')
    sub(solverOptions,'waterDepth', value="8")
    sub(solverOptions,'freesurface', value="0")
    sub(solverOptions,'kBot', value="3.0e6")
    sub(solverOptions,'cBot', value="3.0e5")
    sub(solverOptions,'dtM', value="0.0002")
    sub(solverOptions,'waveKin', value="0")
    sub(solverOptions,'writeUnits', value="yes")
    sub(solverOptions,'frictionCoefficient', value="0")
    sub(solverOptions,'fricDamp', value="200")
    sub(solverOptions,'statDynFricScale', value="1.0")
    sub(solverOptions,'dtIC', value="1.0")
    sub(solverOptions,'cdScaleIC', value="2")
    sub(solverOptions,'threshIC', value="0.001")
    sub(solverOptions,'tmaxIC', value="4")

    mc(moordyn,'bodies')
    sub(bodies,'body',ref='50')

    mc(moordyn,'lines')
    mc(lines,'linedefault')
    sub(linedefault,'ea', value="15E3")
    sub(linedefault,'diameter', value="0.05")
    sub(linedefault,'massDenInAir', value="16.86")
    sub(linedefault,'ba', value="-0.8")
    sub(linedefault,'can', value="1.0")
    sub(linedefault,'cat', value="0.0")
    sub(linedefault,'cdn', value="1.6")
    sub(linedefault,'cdt', value="0.05")
    sub(linedefault,'outputFlags', value="p")

    mc(lines,'line')
    sub(line,'vesselconnection', bodyref="50", x="0", y="0", z=str(connection_z))
    sub(line,'fixconnection', x="-6", y="5.5", z="-8")
    sub(line,'length', value=str(mooring_length), comment="(m)")
    sub(line,'segments', value=str(mooring_segements))

    mc(lines,'line')
    sub(line,'vesselconnection', bodyref="50", x="0", y="0", z=str(connection_z))
    sub(line,'fixconnection', x="-6", y="-5.5", z="-8")
    sub(line,'length', value=str(mooring_length), comment="Water depth", units_comment="m")
    sub(line,'segments', value=str(mooring_segements))

    mc(lines,'line')
    sub(line,'vesselconnection', bodyref="50", x="0", y="0", z=str(connection_z))
    sub(line,'fixconnection', x="6", y="5.5", z="-8")
    sub(line,'length', value=str(mooring_length), comment="Water depth", units_comment="m")
    sub(line,'segments', value=str(mooring_segements))

    mc(lines,'line')
    sub(line,'vesselconnection', bodyref="50", x="0", y="0", z=str(connection_z))
    sub(line,'fixconnection', x="6", y="-5.5", z="-8")
    sub(line,'length', value=str(mooring_length), comment="Water depth", units_comment="m")
    sub(line,'segments', value=str(mooring_segements))

    mc(moordyn,'output')
    sub(output,'time', startTime="0",endTime=str(max_time), dtOut=str(save_time))
    sub(output,'tension', type="all")
    sub(output,'position', type="fixed")


def wavepaddle(wave_height,wave_period):
    mc(special,'wavepaddles')
    mc(wavepaddles,'piston')
    sub(piston,'mkbound', value="10" )
    sub(piston,'waveorder', value="2" )
    sub(piston,'start', value="0" )
    sub(piston,'duration', value="0" )
    sub(piston,'depth', value="8" )
    sub(piston,'pistondir', x="1", y="0", z="0" )
    sub(piston,'waveheight', value=str(wave_height) )
    sub(piston,'waveperiod', value=str(wave_period) )
    sub(piston,'phase', value="0" )
    sub(piston,'ramp', value="1" )
    sub(piston,'savemotion', periods="20", periodsteps="20", xpos="50", zpos="-2")

def draw_piston():
    # mc(mainlist, 'setshapemode')
    # setshapemode.text = 'real | dp | bound'
    sub(mainlist,'setmkbound',mk='10')
    mc(mainlist,'drawbox')
    mc(drawbox,'boxfill')
    boxfill.text = 'solid'
    sub(drawbox,'point',x='-30.5',y='-8',z='-8')
    sub(drawbox,'size',x='0.5',y='16',z='15')
    sub(mainlist,'shapeout',file='Piston',reset='true')

# load stl file - if the stl is too
# small you will get a failure from DualSPHysics
# probably saying there is no fluid generated or
# no float available
def set_piston():
    mc(casedef,'motion')
    mc(motion,'objreal')
    objreal.set('ref','10')
    sub(objreal,'begin',mov='1',start='0')
    sub(objreal,'mvnull', id='1')



######################################################################
# Sensor stuff

def gauges(max_time,save_time):
    mc(special,'gauges')
    sensor_defults(max_time,save_time)

    # make_sensor(0,10,10,'z','newsensor3')
    # for x in np.arange(-200,200,10):
    #     make_sensor(x,0,10,'z','sensor_'+str(x))
    # make_sensor(0,-10,10,'z','newsensor5')
    # make_sensor(10,0,10,'x','newsensor')

def sensor_defults(max_time,save_time):
    mc(gauges,'default')
    # sub(default,'_computedt', value="0.01", comment="Time between measurements. 0:all steps (default=TimeOut)", units_comment="s")
    # sub(default,'_computetime', start="0.1", end="0.2", comment="Start and end of measures. (default=simulation time)", units_comment="s")
    sub(default,'output', value="true", comment="Creates CSV files of measurements (default=false)")
    sub(default,'_outputdt', value=str(save_time), comment="Time between output measurements. 0:all steps (default=TimeOut)", units_comment="s")
    sub(default,'_outputtime', start="0", end=str(max_time), comment="Start and end of output measures. (default=simulation time)", units_comment="s")

def make_sensor(x,y,z,dir,name):
    mc(gauges,'swl')
    swl.set('name',name)
    sub(swl,'pointdp', coefdp="0.5", comment="Distance between check points (value=coefdp*Dp)")
    if dir == 'z':
        sub(swl,'point0', x=str(x), y=str(y), z=str(z), comment="Initial point", units_comment="m")
        sub(swl,'point2', x=str(x), y=str(y), z=str(-z), comment="Final point", units_comment="m")
    else:
        raise Exception('This sensor type needs to be developed.  See make_sensor for more information')



#########################################################################3
# make wave tank case with moorings and all - just insert an stl

def make_wavetank_case(
    density = 1025,
    particle_size = 0.2,
    filename = 'Temp_Case_Def',
    open_channel='off', # or on
    float_density=0.95,
    max_time = 1,
    stl_scale=30,
    save_time = 0.2,
    wave_height = 1,
    wave_period = 5,
    mooring_length = 12,
    connection_z = -2,
    stl_z = -2,
    location= '' #'../DualSPHysics_v5.0_BETA/examples/moordyn/03_WavesMoorings3D/'
    ):

    init_case() #start the case xml

    define_consts(density,open_channel)

    init_geo_mode(particle_size)

    draw_float(stl_scale,stl_z) # floating object to be tested
    # TODO: properly set STL size before loading

    set_floatings(float_density) # set object to float (I don't think order m)

    draw_piston() # creates the wavepaddle object

    draw_tank()

    set_piston()

    # execution
    special() #start the category

    wavepaddle(wave_height,wave_period) # add a wave paddle

    moorings(max_time,save_time,mooring_length=mooring_length,connection_z=connection_z) # add moorings

    gauges(max_time,save_time) # start guages
    for y in np.arange(-200,200,10): # loop through all guages
        make_sensor(0,y,10,'z','sensor_'+str(y).zfill(4)) # make a sensor

    parameters(max_time,save_time)

    with open(location+filename+'.xml', 'wb') as f:
        f.write(et.tostring(root,pretty_print=True, xml_declaration=True,  encoding='utf-8'))
    # print('created '+ filename)
    logger.info('created '+filename)




def make_flume_case(
                    density = 1025,
                    particle_size = 0.1,
                    filename = 'Temp_Case_Def',
                    open_channel='on',
                    max_time = 1,
                    save_time = 0.1,
                    stl_scale=1 ,
                    flumesize=2,
                    uniformvelocity=1,
                    location=''   ):
    init_case()
    define_consts(density,open_channel)
    init_geo_mode(particle_size,flumesize)

    # draw
    mc(mainlist, 'setshapemode')
    setshapemode.text = 'dp | bound'
    sub(mainlist,'setdrawmode',mode='full')
    sub(mainlist,'setmkbound',mk='20')
    load_stl(stl_scale)

    draw_flume(particle_size,flumesize)


    # special (relaxation zone and gauges)
    special()

    #TODO : relaxation zones!!
    rz_uniform(particle_size,size_for_location=flumesize,velocity=uniformvelocity)
    # rz_waves()


    # gauges(max_time,save_time) # start guages
    # ynum = 3
    # xnum = 3
    # margin = flumesize/20
    # for y in np.linspace(-flumesize/2+margin,flumesize/2-margin,ynum): # loop through all guages
    #     for x in np.linspace(-flumesize/2+margin,flumesize/2-margin,xnum):
    #         make_sensor(x,y,flumesize,'z','sls_'+str(x).zfill(4)+'_'+str(y).zfill(4)) # make a sensor


    # params
    parameters(flumesize*1.25,max_time,save_time)

    # close
    with open(location+filename+'.xml', 'wb') as f:
        f.write(et.tostring(root,pretty_print=True, xml_declaration=True,  encoding='UTF-8'))
    logger.info('created '+filename)
