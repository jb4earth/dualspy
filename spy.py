import numpy as np
np.warnings.filterwarnings('ignore')
import matplotlib.pyplot as plt

import os
os.environ['TF_CPP_MIN_LOG_LEVEL'] = '2'
import tensorflow as tf
import ray
import t3f
from loguru import logger

# Fix seed so that the results are reproducable during testing
# tf.random.set_seed(0)
# np.random.seed(0)

def space2explore(lo,hi,res):
    """Creates a zip of a 3d space to explore with
    extents of lo to hi and resolution res."""
    space = make_grid(lo,hi,res)
    shaddow = make_grid(0,res-1,res) #holds the indices for reference in ml process
    return np.vstack((space,shaddow)).T #stacks and transposes for easy iteration

def make_grid(lo,hi,res):
    """makes a 3d grid and returns a zipable version"""
    x = np.linspace(lo,hi,res)
    y = np.linspace(lo,hi,res)
    z = np.linspace(lo,hi,res)
    X,Y,Z = np.meshgrid(x,y,z)
    return [X.flatten(),Y.flatten(),Z.flatten()]

def find_farthest_in_set(points,axis=0):
    """takes an array of points (samples x dims) and spitts out
    the farthest point from all other points"""
    size = points.shape[axis]
    distances = np.zeros((size,size))
    for i,point1 in enumerate(points):
        for j,point2 in enumerate(points):
            a = point2[0] - point1[0]
            b = point2[1] - point1[1]
            c = point2[2] - point1[2]
            distances[i,j]= np.sqrt(a**2+b**2+c**2)
    # print(distances.shape)
    disum = distances.sum(axis=0)
    return np.where(disum == np.amax(disum))

def find_farthest_out_set(inside,outside,axis=0):
    """takes an array of points (samples x dims) and spitts out
    the farthest point from all other points"""
    size1 = inside.shape[axis]
    size2 = outside.shape[axis]
    distances = np.zeros((size1,size2))
    for i,point1 in enumerate(inside):
        for j,point2 in enumerate(outside):
            a = point2[0] - point1[0]
            b = point2[1] - point1[1]
            c = point2[2] - point1[2]
            distances[i,j]= np.sqrt(a**2+b**2+c**2)
#     print(distances.shape)
    disum = distances.sum(axis=0)
    # print(disum)
    return np.where(disum == np.amax(disum))

def find_closest_out_set(inside,outside,axis=0):
    """takes an array of points (samples x dims) and spitts out
    the closest point available to the referenced points"""
    size1 = inside.shape[axis]
    size2 = outside.shape[axis]
    distances = np.zeros((size1,size2))
    for i,point1 in enumerate(inside):
        for j,point2 in enumerate(outside):
            a = point2[0] - point1[0]
            b = point2[1] - point1[1]
            c = point2[2] - point1[2]
            distances[i,j]= np.sqrt(a**2+b**2+c**2)
#     print(distances.shape)
    disum = distances.sum(axis=0)
    # print(disum)
    return np.where(disum == np.amin(disum))

# not exactly sure how this loss is working
def loss(model,prediction_index,observed_values):
    """loss function for learning model (ai) using t3f"""
    estimated_vals = t3f.gather_nd(model,prediction_index)
    return tf.reduce_mean((estimated_vals - observed_values) ** 2)



def step(optimizer,model,index, values):
    """training step for learning model (ai) using t3f"""
    with tf.GradientTape() as tape:
        loss_value = loss(model,index,values)
    gradients = tape.gradient(loss_value, model.tt_cores)
    optimizer.apply_gradients(zip(gradients, model.tt_cores))
    return loss_value

def set_error(guess,truth):
    """using absolute error since relative results in divide by zero errors"""
    abs_error = abs((guess-truth))
    return abs_error

def total_error(ran_cases,predictions,n_cases=1):
    """returns the mean error and index of n_cases worst case(s)"""
    error = set_error(predictions,ran_cases[:,6])
    rc = np.hstack((ran_cases[:,:-1],predictions[:,np.newaxis]))
    rc = np.hstack((rc,error[:,np.newaxis]))
    rc = rc[rc[:,8].argsort()] # sorting by error
    return np.nanmean(rc[:,8]),rc[-n_cases,3:6]

def train_n_predict(optimizer,model,ran_cases,indices,values,iterations):
    """train the model and make predictions
       To collect the values of a TT tensor
       (withour forming the full tensor)
       we use the function t3f.gather_nd"""
    for i in range(iterations):
        loss_v = step(optimizer,model,indices,values).numpy()
    predictions = np.array(t3f.gather_nd(model,ran_cases[:,3:6].astype(int)))
    return predictions


def spy(cases,ran,models,ops,iters=10):
    """pick what cases to run next based on spy_ai
    (machine learning high-error region finder)"""

    # split training and test 50/50
    halfway = int(len(ran)/2)
    test = np.array(ran[:halfway])
    train = np.array(ran[halfway:])
    ran_cases = np.array(ran)

    # crop observed data for training
    t_ind = train[:,3:6].astype(int)
    t_val = train[:,6]


    data = spy_run_ai(ran_cases,ops,models,t_ind,t_val,iters)

    cases,next_cases,errors = spy_report(data,cases)

    return cases,next_cases,errors



def spy_report(data,cases):
    """look at what the ai did, make changes to the case order"""
    # given the worst points, find the best place to check
    # must be in cases since these are the available cases
    # should be close to the bad points in runs

    # todo implement something like Annoy (from spotify)
    # for better nearest negihbor searching

    worst_points = data[:,1]

    next_cases = []
    for point in worst_points:
        case_number = np.abs(np.array(cases[:,3:6]) - point).argmin()
        #todo - how good even is this point finder??
        case = cases[case_number]
        cases = np.delete(cases,case_number,0)
        next_cases.append(case)

    errors = data[:,0]

    return cases,next_cases,errors

# @ray.remote
def training_error(ran_cases,opt,model,t_ind,t_val,iters):
    """train, predict, get the error"""
    predictions = train_n_predict(opt,model,ran_cases,t_ind,t_val,iters)
    # print(predictions.shape,t_ind.shape,t_val.shape)
    tot_err, worst_ind = total_error(ran_cases,predictions)
    return [tot_err, worst_ind, predictions]


def spy_run_ai(ran_cases,ops,models,t_ind,t_val,iters,ray_on=False):
    """run all ML models in parallel"""
    # todo ray_on == False does not work!
    if ray_on == True:
        try:
            ray.init(log_to_driver=False)
        except:
            ray.shutdown()
            ray.init(log_to_driver=False)
        data = []
        for model,op in zip(models,ops):
            data.append(training_error.remote(
                                ran_cases,
                                op,model,
                                t_ind,t_val,
                                iters) )
        data = np.array(ray.get(data))
        ray.shutdown()
    else:
        data = []
        for model,op in zip(models,ops):
            data.append(training_error(
                                ran_cases,
                                op,model,
                                t_ind,t_val,
                                iters) )
        data = np.array(data)

    return data

def spy_set_case(cases,next_cases):
    """sets the next case to be run based on previous accounting"""
    # set the case
    if len(next_cases) > 0: # if cases were set by the spy
        case = next_cases.pop(0)
    else: # nothing set or just starting up
        np.random.shuffle(cases)
        case = cases[0] # just grab the first case
        cases = np.delete(cases,0,0) # delete the case (for accounting)
    return case, cases, next_cases

def spy_init_ai(space_res,learning_rate = 0.15):
    """initialize the machine learning models"""
# todo initialize models in one line (models and ops combined)
# define ML models
    sr_shape = (space_res,space_res,space_res)
# optimizer options from Keras are SGD, RMSprop, Adam, Adadelta, Adagrad, Adamax, Nadam, Ftrl
    op0 = tf.keras.optimizers.Adagrad(learning_rate=learning_rate)
    op1 = tf.keras.optimizers.Adam(learning_rate=learning_rate)
    op2 = tf.keras.optimizers.RMSprop(learning_rate=learning_rate)
    op3 = tf.keras.optimizers.SGD(learning_rate=learning_rate)
    model0 = t3f.get_variable('model0', initializer=t3f.random_tensor(sr_shape, tt_rank=10))
    model1 = t3f.get_variable('model1', initializer=t3f.random_tensor(sr_shape, tt_rank=10))
    model2 = t3f.get_variable('model2', initializer=t3f.random_tensor(sr_shape, tt_rank=10))
    model3 = t3f.get_variable('model3', initializer=t3f.random_tensor(sr_shape, tt_rank=10))

    models = [model0,model1,model2,model3]
    ops = [op0,op1,op2,op3]

    return models, ops

# faster_step = tf.function(step) #todo move this somewhere useful #todo make this work again
