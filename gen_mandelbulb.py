import numpy as np
np.warnings.filterwarnings('ignore')

import pyvista as pv
import open3d as o3

def gridgen(points):
    x_ = np.linspace(-1, 1, points)
    y_ = np.linspace(-1, 1, points)
    z_ = np.linspace(-1, 1, points)

    x, y, z = np.meshgrid(x_, y_, z_, indexing='ij')

    return np.array([x.flatten(),y.flatten(),z.flatten()]).T
def get_rad(v): return np.sqrt(v[:,0]**2+v[:,1]**2+v[:,2]**2)

def get_phi(v): return np.arctan(v[:,1]/v[:,0])

def get_the(v): return np.arctan(np.sqrt(v[:,0]**2+v[:,1]**2)/v[:,2])

def mandelbulb_iter(v,n=[2,2,2]):
    radius = get_rad(v)
    phi = get_phi(v)
    theta = get_the(v)

    x = radius**n[0]*np.sin(theta*n[0])*np.cos(phi*n[0])
    y = radius**n[1]*np.sin(phi*n[1])*np.sin(theta*n[1])
    z = radius**n[2]*np.cos(theta*n[2])

    return np.array([x,y,z]).T

def dropnans(i10,mm=2):
    i10[i10>mm] = np.NaN
    i10[i10<-mm] = np.NaN
    return i10[~np.isnan(i10).any(axis=1)]

def mandelpoints(iters,points,n=[3,3,3]):
    for i in range(iters):
        points += mandelbulb_iter(points,n)
        points = dropnans(points)
    return points


def mandelmesh(bulb,thick=0.3,smooth=150,decimate=0.5):
    dbulb = bulb*0 # create matching zero vector

    # create cube
    cubecorners = np.array([[1,1,1],[1,1,-1],[1,-1,1],[-1,1,1],[-1,-1,1],[-1,1,-1],[1,-1,-1],[-1,-1,-1]])
    s = thick*cubecorners # scale cubes

    # cubify mandelbulb (it's a pointcloud with no-normals so this is needed)
    ex = np.vstack([bulb+s[0],bulb+s[1],bulb+s[2],bulb+s[3],bulb+s[4],bulb+s[5],bulb+s[6],bulb+s[7]])

    # convert to open3d Point Cloud
    pcd = o3.geometry.PointCloud()
    pcd.points = o3.utility.Vector3dVector(ex)

    # set normals
    s = s*(1/thick)
    nnn =  np.vstack([dbulb+s[0],dbulb+s[1],dbulb+s[2],dbulb+s[3],dbulb+s[4],dbulb+s[5],dbulb+s[6],dbulb+s[7]])
    pcd.normals = o3.utility.Vector3dVector(nnn)
    pcd.normalize_normals()

    # create mesh from points (poisson reconstruction)
    test,_ = o3.geometry.TriangleMesh.create_from_point_cloud_poisson(pcd,depth=6,scale=2.25)

    # transfer to pyvista for cleaning and viewing
    o3.io.write_triangle_mesh('Temp.ply',test)
    mesh = pv.read('Temp.ply')

    # pyvista cleaning
    mesh.extract_largest(True)
    mesh = mesh.decimate(decimate)
    mesh = mesh.smooth(n_iter=smooth)
    return mesh

# generate the mandelbulb points
points = mandelpoints(64,gridgen(100),[-7,-5,-2.5])
mesh = mandelmesh(points,0.25,40,0)

pv.save_meshio('Temp.stl',mesh)


# # visualization
# mesh['values'] = (mesh.points[:,2]) # color by image depth (depends on camera position)
# p = pv.BackgroundPlotter()
# p.add_mesh(mesh,scalars=mesh['values'],cmap='fire')
# p.link_views()
# p.camera_position = [-180,0,180] # initial camera position
# p.show()
