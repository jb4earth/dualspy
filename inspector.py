import pyvista as pv
import operator
import ray
import glob
from loguru import logger
import numpy as np

def particles(frames,num_cells=5,location='*/particles/*.vtk',ray_on=False):
    """get the last frames, discritize and report"""
    files = get_xfiles(frames,location)
    logger.trace(str(files))
    logger.trace('some file names should be listed above')

    results = []
    logger.trace('===============\nmade it to the loop in particles')
    if ray_on==True:
        ray.init(log_to_driver=False)
        for file in files:
            results.append(particle_discretize.remote(file,num_cells))
        result = np.array(ray.get(results))
        ray.shutdown()
        return result
    else:
        for file in files:
            results.append(particle_discretize(file,num_cells))
        return np.array(results)

def get_xfiles(frames, folder):
    """get the top frames files """
    list_of_files = glob.glob(folder) # * means all if need specific format then *.csv
    return sorted(list_of_files)[-frames:]

# one file at a time, all cells in serial
# @ray.remote
def particle_discretize(file,num_cells,vlmag = ['Idp','Rhop','Press'], vl3d = ['Vel','Vor']):
    """cut up a 3d domain into equal sized cells"""
    logger.trace('\n++++++++\nIn Ray Particle Disc\n+++++++++++')
    mesh = pv.read(file) # get mesh`
    bounds = cell_bounds(num_cells) # set bounds
    logger.trace(bounds)
    # call in the workers
    res3d = particle_discretize_3d(vl3d,mesh,bounds)
    resmag = particle_discretize_mag(vlmag,mesh,bounds)
    logger.trace('\n'+str(res3d.shape)+str(resmag.shape))
    logger.trace([res3d,resmag])
    results =  np.concatenate((resmag,res3d),axis=1)
    return results

# one file at a time, all values
def particle_discretize_3d(val_list,mesh,bounds):
    """3d vatiable discretize and report"""
    # logger.trace(str(tuple(len(val_list))+tuple(6)+tuple(bounds.shape[0])))
    results = np.zeros(tuple([bounds.shape[0]])+tuple([len(val_list)])+tuple([2,3]))
    logger.trace(str(results.shape))
    # results = np.empty(tuple(len(val_list))+tuple(6)+tuple(bounds.shape[0])) # init results array
    for i,val_str in enumerate(val_list): #loop through all requested variables
        mesh.set_active_scalars(val_str) # set the variable to active
        for j,bound in enumerate(bounds): # loop through all cells
            logger.trace(bound)
            clipped = mesh.clip_box(bound,invert=False) # clip to cell size
            # add results to one big array
            # logger.trace(str(abs(clipped[val_str]).mean()))
            logger.trace('\n--------------\npv clip\n--------------')
            logger.trace([clipped,bound])
            results[j,i,0,:] = np.nanmean(abs(clipped[val_str]),axis=0)
            results[j,i,1,:] = np.nanstd(abs(clipped[val_str]),axis=0)

            # results[i,0,j] = (abs(clipped[val_str][0])).mean()
            # results[i,1,j] = (abs(clipped[val_str][1])).mean()
            # results[i,2,j] = (abs(clipped[val_str][2])).mean()
            # results[i,3,j] = clipped[val_str][0].std()
            # results[i,4,j] = clipped[val_str][1].std()
            # results[i,5,j] = clipped[val_str][2].std()
    return results

def particle_discretize_mag(val_list,mesh,bounds):
    """magnitude variable discretize and report"""
    results = np.zeros(tuple([bounds.shape[0]])+tuple([len(val_list)])+tuple([2,3]))
    logger.trace(str(results.shape))
    for i,val_str in enumerate(val_list): #loop through all requested variables
        mesh.set_active_scalars(val_str) # set the variable to active
        for j,bound in enumerate(bounds): # loop through all cells
            clipped = mesh.clip_box(tuple(bound),invert=False) # clip to cell size
            # add results to one big array
            logger.trace(clipped)
            logger.trace(bound)
            results[j,i,0,0] = np.nanmean(abs(clipped[val_str]))
            results[j,i,1,0] = np.nanstd(abs(clipped[val_str]))
    return results


def cell_bounds(num_cells):
    """find the boundaries of the cells in 3d"""
    # add one since cells have 2 boundaries in each dim
    X = np.linspace(-1,1,num_cells+1)
    Y = np.linspace(-1,1,num_cells+1)
    Z = np.linspace(-1,1,num_cells+1)
    x, y, z = np.meshgrid(X,Y,Z)

    # find the scale in each dim
    x_dim = abs(x[0,0,0] - x[1,1,1])
    y_dim = abs(y[0,0,0] - y[1,1,1])
    z_dim = abs(z[0,0,0] - z[1,1,1])
    x_dim, y_dim, z_dim

    # we go one less point in each dimmension, then use the scale to grab create the box size
    # todo schange this if you change discritization scheme
    zipped = zip((x[:-1,:-1,:-1]).flatten(),(y[:-1,:-1,:-1]).flatten(),(z[:-1,:-1,:-1]).flatten())
    list_return = []
    for item in zipped:
        shuf_tup = item+tuple(map(operator.add, item, (x_dim,y_dim,z_dim)))
        # this tuple is in the wrong order! fixing
        unshuf_tup = [shuf_tup[0],shuf_tup[3],shuf_tup[1],shuf_tup[4],shuf_tup[2],shuf_tup[5]]
        list_return.append(unshuf_tup)

    # np.array(list_return).shape
    return np.array(list_return)

