

# About DualSpy

DualSpy is a a smart supervisor for DualSPHysics experiments developed in Python. Experiments can range from shape optimizations to physics exploration as long as there is an unreasonable amount of cases to run through DualSPHysics, DualSpy is worth using.  The smart supervisor uses machine learning to find pick the best cases to run to explore a given experiment space.  More information on designing experiments for DualSpy can be found in the Experiments folder readme.

# Running the Software

Project dependencies can be installed by running:

`conda env create -f conda_env.yaml`


On Linux you'll need to give DualSPH permission to run with:

`chmod +x linux_gpu.sh`

`chmod +x linux_cpu.sh`


to run the software:

`python duals.py cpu experiment_name`

`python duals.py gpu experiment_name`


You can set the default in `duals.py`

# Documentation
All functions should be documented where they are found. There is some additional jupyter notebooks which will help you to understand what is going on in the documentation folder.

## TODOs
- gauges, mooring, floating info need to be analyzed and pumped into npy files (right now we only look at fluid properties)
- implement some nearest neighbor search library like Annoy (by Spotify)

## Current Software Weaknesses:
Error tracing in the runtime is difficult. Get used to DualSPHysics very long reports.  You may need to extend the number of lines/characters that your terminal can display.

Running on the HPC is a pain.  The xml generator may not work. If experiment does not change the domain with each run (the stl file can still change), you can pull the xml generator out.

### DualSPHysics weaknesses:
Be careful when using stl files for shapes.  If the particle size is not small enough, no geometry will be created and in turn no fluid will be created.

In the same vain, watch out where the fluid is being 'seeded'.  If it is not in the domain you're in for some zero depth errors.

# Author

This software was written by John Brindley. Contact jb4earth at g mail dot com if you have questions (after you read the docs of course ;)

# License
MIT (dependencies may have their own license types)
