
#!/usr/bin/env python3

# logging
import sys
from loguru import logger
logger.remove()
logger.add(sys.stderr, level="ERROR")

logger.trace('loading halo')
# progress bar
from halo import Halo
spinner = Halo(text='Initializing ', spinner='moon')
spyspin = {
  'interval': 100,
  'frames': ['🕵🏿‍♀️','🕵🏾‍♀️','🕵️‍♂️','🕵️‍♀️','🕵🏽‍♀️']
}
spinner1 = Halo(text='Running case ', spinner='dots')
spinner2 = Halo(text='Spying on cases ', spinner='dots')
spinner.start()

logger.trace('loading externals')
#timer
from time import sleep, perf_counter, ctime, time

# externals
import os
from os import path
import sys
import numpy as np
import glob
import subprocess
# import sys
import shutil
import pyvista as pv
import glob
from pymeshfix import _meshfix


logger.trace('loading project libs')
# project stuff
sys.path.append(".")
from gen_id import idgen
# from analysis import *
from gen_mandelbulb import *
from analyze import particles
from spy import *




logger.trace('parsing args')
# arparge - set the hardware typeparaview
import argparse
parser = argparse.ArgumentParser(description='A python supervisor for DualSPHysics experiments.')
parser.add_argument('hardware_type', type=str, help='cpu or gpu (lowercase only)')
parser.add_argument('experiment_name', type=str, help='experiment name')
args = parser.parse_args()


def run_dualsph(hardware_type):
    logger.trace('running dualsph')
    if hardware_type == 'gpu':
        dualsph = './linux_gpu.sh'
    else:
        dualsph = './linux_cpu.sh'
    # subprocess.run(dualsph,shell=True)#,check=True,stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    dsph = subprocess.run(dualsph,shell=True,check=True,stdout=subprocess.PIPE, stderr=subprocess.STDOUT)
    logger.trace(dsph.stdout)
    logger.debug(dsph.stderr)
    logger.debug('dualsph run complete')
    # bar.next()

def gen_stl(Va,Vb,Vc,iters):
    rm_file('Temp.stl')

    points = mandelpoints(iters,gridgen(100),[Va,Vb,Vc])
    points = points - points.mean(axis=0) #center
    points = points * (1/abs(points).max(axis=0)) # scale to cubeish size
    mesh = mandelmesh(points,0.15,40,0.95)

    pv.save_meshio('Temp.stl',mesh)
    _meshfix.clean_from_file('Temp.stl','Temp.stl')

    # bar.next()

# def gen_case():


def analyze_vtks(frames,bulbid):
    """analyzes case and returns resuls in of variables
    IDp, Rhop, Press, Vel, Vor of shape
    (frames, cells, variable, (mean,std), (x,y,z))
    """
    logger.trace('+++++++++++++++++++\nAnalyzing\n++++++++++++++++')

    results = particles(frames)
    logger.trace(results)
    logger.trace('the shape is (frames, cells, variable, (mean,std), (x,y,z))')
    logger.trace(results.shape)
    np.save('experiments/'+args.experiment_name+'/particles_'+bulbid+'.npy',results)
    # todo surface sensors read from csv
    # todo mooring read from csv

    # head = []
    # results, head = surfaces(frames,res)
    # np.save('surfaces_header.npy',head)
    # np.save('npy/surfaces_'+bulbid+'.npy',results)

    logger.trace('results saved')
    return results
    # bar.next()

def rm_file(myfile):
    """try to remove a file"""
    try:
        os.remove(myfile)
        logger.debug(myfile + ' = deleted')
    except:  ## if failed, report it back to the user ##
        logger.debug(myfile + ' = does not exist to be deleted')


def spy_on_what(results, ran, case):
    """lets spy on net vorticity to start"""
    result = np.nanmean(results,(0,1))[4]
    # todo, observed is coming back as a nan sometimes...
    observed_vorticity = np.sqrt(result[0,0]**2+result[0,1]**2+result[0,2]**2)
    if np.isnan(observed_vorticity) :
        logger.error('\n-----NAN found-----')
        logger.error(case)
        logger.error(np.nanmean(results,(0,1)))
        logger.error(result[0])
    ran.append(np.hstack((case,observed_vorticity,1)))
    return ran, observed_vorticity

##############################################################
##############################################################
##############################################################
##############################################################
##############################################################
##############################################################
##############################################################
##############################################################


logger.trace('initializing experiment')

space_res = 20
cases = space2explore(-10,10,space_res)
models, ops = spy_init_ai(space_res)


# todo init from case
next_cases, ran, error_log = [], [], []

# see if there are already any cases and if so load them
if not os.path.exists('experiments/'+args.experiment_name):
    os.makedirs('experiments/'+args.experiment_name)
else:
    already_run = glob.glob('experiments/'+args.experiment_name+'/man*particles*.npy')
    # logger.error(already_run)
    for case in already_run:
        caseparts = case.split('_')
        shift = 1
        # logger.error(case)
        caseplace = np.array([float(i) for i in [caseparts[shift+2],caseparts[shift+3],caseparts[shift+4]]])
        # logger.error(caseplace)
        casenumber = 0
        for future_case in cases:
            # logger.error([future_case[0:3],caseplace])
            if (future_case[0:3] == caseplace).all():
                ran.append(spy_on_what(np.load(case),ran,case))
                cases = np.delete(cases,casenumber,0)
            casenumber += 1


    spinner.succeed('Loaded: '+str(len(ran)) + ' cases')
    spinner.stop()
while len(cases) > 0:
    spinner1.start()

    logger.trace('setting cases')
    case, cases, next_cases = spy_set_case(cases,next_cases)
## Create STL
    logger.trace('generating stl')
    Va,Vb,Vc = case[0:3]
    iters = 3
    gen_stl(Va,Vb,Vc,iters)
    # TODO: check minimum particle size per geo

## Generate cases
    logger.trace('generating case')
    exec(open('gen_case.py').read())
    # this ugly import must be done this way and cannot be nested
    # because of the way we are using globals to generate xml
    # global use changes the 'scope' thus calling for this special case import
    # make_wavetank_case(
    #     particle_size = 0.2,
    #     density = 1000,
    #     stl_scale = 4,
    #     float_density = 1,
    #     max_time=50,
    #     save_time = 0.1,
    #     wave_height = 1,
    #     wave_period = 5,
    #     mooring_length = 9.1,
    #     connection_z = -4,
    #     stl_z=-4,
    #     location = ''
    #     )

    make_flume_case(
                        density = 1025,
                        particle_size = 0.15,
                        filename = args.experiment_name,
                        open_channel='on',
                        max_time = 2,
                        save_time = 0.1,
                        stl_scale=0.5 ,
                        flumesize=2, #can't change without adjusting analyze todo
                        uniformvelocity=1.0,
                        location='experiments/'+args.experiment_name+'/'   )
    # sets the active case
    os.popen('cp experiments/'+args.experiment_name+'/'+args.experiment_name+'.xml Temp_Case_Def.xml')

## Run DualSPHysics
    logger.trace('running dualsph')
    run_dualsph(args.hardware_type)

## Anayze # of frames
    logger.trace('running analysis')
    results = analyze_vtks(10,idgen(Va,Vb,Vc,iters))

    # break
    # surfaces needs to be a csv reader todo

## Spy time
    logger.trace('running spy')

    ran, observed_values = spy_on_what(results, ran, case)

    spinner1.succeed('Run completed ')
    spinner1.stop()

    logger.error(observed_values)

    if len(ran) % 4 == 0:
        spinner2.start()

        cases, next_cases, errors = spy(cases,ran,models,ops,iters=100)
        error_log.append(errors)

        spinner2.succeed('Spying done ')
        spinner2.stop()
        logger.error('\n\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'
        +'\n+++++++++++++++++++++++= SPY AI ERROR =+++++++++++++++++++++++++'
        +'\n'+str(errors)
        +'\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'
        +'\ncases run: '+str(len(ran))+'of'+str(space_res**3)
        +'\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'
        +'\n++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++++'
        )

        # breaking criteria
        # todo make new models if things aren't going well, change the learn rate and iterations
        if (np.array(error_log))[-3:].mean() < 0.1:
            break
        # elif errors.min() < 0.01:
        #     break
        else:
            # spinner1.start()
            continue
    # spinner1.start()

##  counting
#    i += 1
#    stop = perf_counter()
#    remaining = ctime(time()+(len(cases)*(stop-start)*(1-i/len(cases))))
#    logger.critical('time remaining: ' + str(remaining))
   # break
# bar.finish()
